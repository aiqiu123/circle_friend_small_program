const checkType = (str, type) => {
  switch (type) {
    case 'email':
      return /^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/.test(str)
    case 'mobile':
      return /^1[3|4|5|7|8][0-9]{9}$/.test(str)
    case 'tel':
      return /^(0\d{2,3}-\d{7,8})(-\d{1,4})?$/.test(str)
    case 'number':
      return /^[0-9]$/.test(str)
    case 'english':
      return /^[a-zA-Z]+$/.test(str)
    case 'text':
      return /^\w+$/.test(str)
    case 'chinese':
      return /^[\u4E00-\u9FA5]+$/.test(str)
    case 'lower':
      return /^[a-z]+$/.test(str)
    case 'upper':
      return /^[A-Z]+$/.test(str)
    default:
      return true
  }
}

const _now = () => {
  const date = new Date()
  const dayStr = `${date.getDate()}`
  const monthStr = `${date.getMonth() + 1}`
  const day = dayStr.length > 1 ? dayStr : `0${dayStr}`
  const month = monthStr.length > 1 ? monthStr : `0${monthStr}`
  return `${date.getFullYear()}-${month}-${day}`
}

export default {
  checkType,
  _now
}
