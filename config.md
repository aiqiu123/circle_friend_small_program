# Java 环境配置
alias setJdk8='export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_131.jdk/Contents/Home'
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_131.jdk/Contents/Home
export CLASSPATH=.:$JAVE_HOME/lib/tools.jar
export PATH="$PATH:$JAVA_HOME/bin"

# Android sdk
export ANDROID_HOME=/Users/turbo/Library/Android/sdk
export PATH="$PATH:$ANDROID_HOME/tools"
export PATH="$PATH:$ANDROID_HOME/platform-tools"
export PATH="/usr/local/opt/icu4c/bin:$PATH"
export PATH="/usr/local/opt/icu4c/sbin:$PATH"

export ANDROID_NDK=/Users/turbo/Library/Android/ndk
export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
