1.首页轮播图
GET:{url}/banner
例如:
http://qyh.local/banner
返回:
有数据:
[
    {
        "id": 2,
        "title": "首图2",
        "type": 1,
        "start_time": "2018-05-30 00:13:12",
        "end_time": "2018-05-30 00:13:12",
        "url": "/company/1",
        "image_url": "https://qyh.local/uploads/images/20180530/82289100_1527610389.jpg",
        "content": "",
        "sort": 0,
        "position_id": 0,
        "status": 1,
        "created_at": "2018-05-30 00:13:12",
        "updated_at": "2018-05-30 00:13:12",
        "click_num": 0
    },
    {
        "id": 1,
        "title": "首图1",
        "type": 1,
        "start_time": "2018-05-30 00:12:09",
        "end_time": "2018-05-30 00:12:09",
        "url": "#",
        "image_url": "https://qyh.local/uploads/images/20180530/9435200_1527610327.jpg",
        "content": "",
        "sort": 0,
        "position_id": 0,
        "status": 1,
        "created_at": "2018-05-30 00:12:09",
        "updated_at": "2018-05-30 00:12:09",
        "click_num": 0
    }
]

无数据
[]

2.商家获取用户信息
POST:
http://{url}/user/user_info/{id}
{_TOKEN_:''}
例如:
POST: http://qyh.local/user/user_info/4
{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5}
有数据：
{
    "id": 2,
    "id_sn": "S000002",
    "name": "User_2",
    "avatar": "http://wx.qlogo.cn/mmopen/vi_32/aSKcBBPpibyKNicHNTMM0qJVh8Kjgiak2AHWr8MHM4WgMEm7GFhsf8OYrySdbvAMvTsw3mo8ibKicsnfN5pRjl1p8HQ/0",
    "nick_name": "User_2",
    "open_id": "KKGZUI0egBJY1zhBYw2KhdUfwVJJE",
    "true_name": "",
    "phone": "",
    "user_type": 1,
    "id_card": "370124*********4322",
    "status": 0,
    "consumption": "10000.00",
    "is_auto_add": 0
}

无数据:
{
    "status": 1,
    "return": "未找到该用户!",
    "data": "未找到该用户!"
}



3. 通过ID获取用户信息
POST:
http://{url}/user/user_info_sn
{_TOKEN_:'',id_sn:''}
例如:
POST: http://qyh.local/user/user_info_sn/
{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5,id_sn:S000002}
有数据：
{
    "id": 2,
    "id_sn": "S000002",
    "name": "User_2",
    "avatar": "http://wx.qlogo.cn/mmopen/vi_32/aSKcBBPpibyKNicHNTMM0qJVh8Kjgiak2AHWr8MHM4WgMEm7GFhsf8OYrySdbvAMvTsw3mo8ibKicsnfN5pRjl1p8HQ/0",
    "nick_name": "User_2",
    "open_id": "KKGZUI0egBJY1zhBYw2KhdUfwVJJE",
    "true_name": "",
    "phone": "",
    "user_type": 1,
    "id_card": "370124*********4322",
    "status": 0,
    "consumption": "10000.00",
    "is_auto_add": 0
}

无数据:
{
    "status": 1,
    "return": "未找到该用户!",
    "data": "未找到该用户!"
}


4.用户额度共享
http://{url}/user/share/{id}
{_TOKEN_:'',money:''}
例如:
POST: http://qyh.local/user/share/2
{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5,money:10000}
成功:
{
    "status": 0,
    "return": "操作成功!",
    "data": "操作成功!"
}

失败:
{
    "status": 1,
    "return": "额度不能低于1元",
    "data": "额度不能低于1元"
}


5.用户额度信息(商家授权,共享)

http://{url}/user/quota
{_TOKEN_:''}
例如:
POST: http://qyh.local/user/quota
{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5}
成功:
[
    {
        "id": 1,
        "user_id": 1,
        "company_id": 1,
        "company_user_id": 2,
        "total_amount": "10000.00",
        "used_amount": "102.00",
        "now_amount": "9898.00",
        "sort": 100,
        "share_type": "商家额度",
        "created_at": "2018-05-27 14:00:03",
        "updated_at": "2018-05-27 14:03:48",
        "discount": 100,
        "user_name": "User_2",
        "company_name": "测试",
        "name": "User_2"
    },
    {
        "id": 2,
        "user_id": 1,
        "company_id": 0,
        "company_user_id": 2,
        "total_amount": "10000.00",
        "used_amount": "102.00",
        "now_amount": "9898.00",
        "sort": 100,
        "share_type": "共享额度",
        "created_at": "2018-05-27 14:00:03",
        "updated_at": "2018-05-27 14:03:48",
        "discount": 100,
        "user_name": "User_2",
        "company_name": "测试",
        "name": "User_2"
    }
]

失败:
[]

6.用户额度详细
POST:{url}/user/quota/{id}  
{_TOKEN_:''}
例如：
POST: http://qyh.local/user/quota/1
{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5}

成功:
{
    "quota": {
        "id": 1,
        "user_id": 1,
        "company_id": 1,
        "company_user_id": 2,
        "total_amount": "10000.00",
        "used_amount": "102.00",
        "now_amount": "9898.00",
        "sort": 100,
        "share_type": 0,
        "created_at": "2018-05-27 14:00:03",
        "updated_at": "2018-05-27 14:03:48",
        "discount": 100,
        "share_type_text": "商家授额"
    },
    "company": {
        "id": 1,
        "name": "测试",
        "contact_name": "张明",
        "contact_phone": "13333333333",
        "photo": "/uploads/images/20180522/45308300_1526949454.png",
        "user_id": 2,
        "parent_industry_id": 1,
        "industry_id": 2,
        "position_x": "",
        "position_y": "",
        "status": 1,
        "province": 14,
        "city": 208,
        "district": 1750,
        "address": "测试",
        "company_info": "测试",
        "is_check": 0,
        "reject_reason": "",
        "license_no": "",
        "license_photo": "/uploads/images/20180522/15763600_1526949465.png",
        "pay_password": "",
        "created_at": "2018-05-22 08:43:52",
        "updated_at": "2018-05-22 08:43:52",
        "details": "<p>测试测试</p>"
    },
    "user": {
        "id": 2,
        "id_sn": "S000002",
        "name": "User_2",
        "avatar": "http://wx.qlogo.cn/mmopen/vi_32/aSKcBBPpibyKNicHNTMM0qJVh8Kjgiak2AHWr8MHM4WgMEm7GFhsf8OYrySdbvAMvTsw3mo8ibKicsnfN5pRjl1p8HQ/0",
        "nick_name": "User_2",
        "open_id": "KKGZUI0egBJY1zhBYw2KhdUfwVJJE",
        "true_name": "",
        "phone": "",
        "user_type": 1,
        "id_card": "370124*********4322",
        "status": 0,
        "consumption": "10000.00",
        "is_auto_add": 0
    }
}

无信息:
{
    "status": 1,
    "return": "未找到该信息!",
    "data": "未找到该信息!"
}


7.用户额度排序(sort值越大越靠前)
POST:{url}/user/quota_sort/{id}
{_TOKEN_:'',sort:''}
例如：
POST: http://qyh.local/user/quota_sort/1
{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5,sort:1000}

操作成功:
{
    "status": 0,
    "return": "修改成功!",
    "data": "修改成功!"
}

操作失败:
{
    "status": 1,
    "return": "未找到该信息!",
    "data": "未找到该信息!"
}

8.获取记账二维码(暂时)

POST:{url}/user/iod_code
{_TOKEN_:''}
例如：
POST: http://qyh.local/user/iod_code
{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5}

操作成功:
{
    "status": 0,
    "return": "https://qyh.local/user/qcode/1_appcode.png",
    "data": "https://qyh.local/user/qcode/1_appcode.png"
}
操作失败:
{
    "status": 1,
    "return": "获取失败!",
    "data": "获取失败!"
}

