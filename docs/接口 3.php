1.用户信息
POST:{url}/user/info
参数{_TOKEN_:''}
例如:
http://qyh.local/user/info
POST:{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5}
返回:
有数据:
{
    "id": 1,
    "id_sn": "S000001",
    "name": "Band",
    "avatar": "http://wx.qlogo.cn/mmopen/vi_32/aSKcBBPpibyKNicHNTMM0qJVh8Kjgiak2AHWr8MHM4WgMEm7GFhsf8OYrySdbvAMvTsw3mo8ibKicsnfN5pRjl1p8HQ/0",
    "nick_name": "Band",
    "open_id": "oGZUI0egBJY1zhBYw2KhdUfwVJJE",
    "true_name": "",
    "phone": "",
    "user_type": 0,
    "id_card": "",
    "status": 0,
    "consumption": "10000.00",
    "is_auto_add": 0,   
    "quota": 0          //可用额度
}

未登录
{
    "status": -3,
    "return": "请先登录",
    "data": "请先登录"
}


2.修改密码

POST:{url}/user/password
参数{_TOKEN_:'',password:'',repassword:''}
例如:
http://qyh.local/password
POST:{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5,password:123456,repassword:123456}
返回:
成功:
{
    "status": 0,
    "return": "密码修改成功!",
    "data": "密码修改成功!"
}

失败:
{
    "status": 1,
    "return": "密码不能少于6位!",
    "data": "密码不能少于6位!"
}



3.获取地区信息

GET:{url}/region/{id}

例如:
获取省份
http://qyh.local/region/1
{
    "2": {
        "region_id": 2,
        "parent_id": 1,
        "region_name": "北京",
        "region_type": 1,
        "is_kaitong": 0,
        "first_char": "B"
    },
    "3": {
        "region_id": 3,
        "parent_id": 1,
        "region_name": "安徽",
        "region_type": 1,
        "is_kaitong": 0,
        "first_char": "A"
    },
    "4": {
        "region_id": 4,
        "parent_id": 1,
        "region_name": "福建",
        "region_type": 1,
        "is_kaitong": 0,
        "first_char": "F"
    },
    "5": {
        "region_id": 5,
        "parent_id": 1,
        "region_name": "甘肃",
        "region_type": 1,
        "is_kaitong": 0,
        "first_char": "G"
    },
    ....
}

获取省下面的市
http://qyh.local/region/3
{
    "36": {
        "region_id": 36,
        "parent_id": 3,
        "region_name": "安庆",
        "region_type": 2,
        "is_kaitong": 0,
        "first_char": "A"
    },
    "37": {
        "region_id": 37,
        "parent_id": 3,
        "region_name": "蚌埠",
        "region_type": 2,
        "is_kaitong": 0,
        "first_char": "B"
    },
    "38": {
        "region_id": 38,
        "parent_id": 3,
        "region_name": "巢湖",
        "region_type": 2,
        "is_kaitong": 0,
        "first_char": "C"
    },
    "39": {
        "region_id": 39,
        "parent_id": 3,
        "region_name": "池州",
        "region_type": 2,
        "is_kaitong": 0,
        "first_char": "C"
    },
    "40": {
        "region_id": 40,
        "parent_id": 3,
        "region_name": "滁州",
        "region_type": 2,
        "is_kaitong": 0,
        "first_char": "C"
    },
    "41": {
        "region_id": 41,
        "parent_id": 3,
        "region_name": "阜阳",
        "region_type": 2,
        "is_kaitong": 0,
        "first_char": "F"
    },
    "42": {
        "region_id": 42,
        "parent_id": 3,
        "region_name": "淮北",
        "region_type": 2,
        "is_kaitong": 0,
        "first_char": "H"
    },
    "43": {
        "region_id": 43,
        "parent_id": 3,
        "region_name": "淮南",
        "region_type": 2,
        "is_kaitong": 0,
        "first_char": "H"
    },
    "44": {
        "region_id": 44,
        "parent_id": 3,
        "region_name": "黄山",
        "region_type": 2,
        "is_kaitong": 0,
        "first_char": "H"
    },
    "45": {
        "region_id": 45,
        "parent_id": 3,
        "region_name": "六安",
        "region_type": 2,
        "is_kaitong": 0,
        "first_char": "L"
    },
    "46": {
        "region_id": 46,
        "parent_id": 3,
        "region_name": "马鞍山",
        "region_type": 2,
        "is_kaitong": 0,
        "first_char": "M"
    },
    "47": {
        "region_id": 47,
        "parent_id": 3,
        "region_name": "宿州",
        "region_type": 2,
        "is_kaitong": 0,
        "first_char": "S"
    },
    "48": {
        "region_id": 48,
        "parent_id": 3,
        "region_name": "铜陵",
        "region_type": 2,
        "is_kaitong": 0,
        "first_char": "T"
    },
    "49": {
        "region_id": 49,
        "parent_id": 3,
        "region_name": "芜湖",
        "region_type": 2,
        "is_kaitong": 0,
        "first_char": "W"
    },
    "50": {
        "region_id": 50,
        "parent_id": 3,
        "region_name": "宣城",
        "region_type": 2,
        "is_kaitong": 0,
        "first_char": "X"
    },
    "51": {
        "region_id": 51,
        "parent_id": 3,
        "region_name": "亳州",
        "region_type": 2,
        "is_kaitong": 0,
        "first_char": "B"
    },
    "3401": {
        "region_id": 3401,
        "parent_id": 3,
        "region_name": "合肥",
        "region_type": 2,
        "is_kaitong": 0,
        "first_char": "H"
    }
}


获取县/区
http://qyh.local/region/36
{
    "398": {
        "region_id": 398,
        "parent_id": 36,
        "region_name": "迎江区",
        "region_type": 3,
        "is_kaitong": 0,
        "first_char": "Y"
    },
    "399": {
        "region_id": 399,
        "parent_id": 36,
        "region_name": "大观区",
        "region_type": 3,
        "is_kaitong": 0,
        "first_char": "D"
    },
    "400": {
        "region_id": 400,
        "parent_id": 36,
        "region_name": "宜秀区",
        "region_type": 3,
        "is_kaitong": 0,
        "first_char": "Y"
    },
    "401": {
        "region_id": 401,
        "parent_id": 36,
        "region_name": "桐城市",
        "region_type": 3,
        "is_kaitong": 0,
        "first_char": "T"
    },
    "402": {
        "region_id": 402,
        "parent_id": 36,
        "region_name": "怀宁县",
        "region_type": 3,
        "is_kaitong": 0,
        "first_char": "H"
    },
    "403": {
        "region_id": 403,
        "parent_id": 36,
        "region_name": "枞阳县",
        "region_type": 3,
        "is_kaitong": 0,
        "first_char": "Z"
    },
    "404": {
        "region_id": 404,
        "parent_id": 36,
        "region_name": "潜山县",
        "region_type": 3,
        "is_kaitong": 0,
        "first_char": "Q"
    },
    "405": {
        "region_id": 405,
        "parent_id": 36,
        "region_name": "太湖县",
        "region_type": 3,
        "is_kaitong": 0,
        "first_char": "T"
    },
    "406": {
        "region_id": 406,
        "parent_id": 36,
        "region_name": "宿松县",
        "region_type": 3,
        "is_kaitong": 0,
        "first_char": "S"
    },
    "407": {
        "region_id": 407,
        "parent_id": 36,
        "region_name": "望江县",
        "region_type": 3,
        "is_kaitong": 0,
        "first_char": "W"
    },
    "408": {
        "region_id": 408,
        "parent_id": 36,
        "region_name": "岳西县",
        "region_type": 3,
        "is_kaitong": 0,
        "first_char": "Y"
    }
}


4.获取行业信息(共两级)

GET:{url}/industry_get/{id}

例如：
获取父级分类行业
http://qyh.local/industry_get/0
{
    "1": {
        "id": 1,
        "parent_id": 0,
        "name": "美食",
        "status": 1,
        "created_at": "2018-05-22 08:15:57",
        "updated_at": "2018-05-22 08:15:57",
        "level": 1
    },
    "3": {
        "id": 3,
        "parent_id": 0,
        "name": "购物",
        "status": 1,
        "created_at": "2018-05-22 08:17:59",
        "updated_at": "2018-05-22 08:17:59",
        "level": 1
    }
}

获取子分类行业

http://qyh.local/industry_get/1
{
    "2": {
        "id": 2,
        "parent_id": 1,
        "name": "火锅",
        "status": 1,
        "created_at": "2018-05-22 08:16:10",
        "updated_at": "2018-05-22 08:17:45",
        "level": 2
    }
}


5.绑定商家
POST:{url}/user/company

参数
{
    _TOKEN_:'',
    position_x:'',  //x坐标
    position_y:'',  //y坐标
    province:'',    
    city:'',
    district:'',
    address:'',     //详细地址
    name:'',        //公司名称
    contact_phone:'',//联系电话
    contact_name:'',//联系人名称
    license_photo:'',//营业照片
    license_no:'' ,  //营业执照号码
    parent_industry_id:'',
    industry_id:''

}



例如:
http://qyh.local/user/company
{    
    _TOKEN_:'bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5',
    position_x:'116.260827',  //x坐标
    position_y:'39.941493',  //y坐标
    province:'2',    
    city:'52',
    district:'500',
    address:'测试街道',     //详细地址
    name:'永旺超市',        //公司名称
    contact_phone:'1333333333',//联系电话
    contact_name:'张三',//联系人名称
    license_photo:'http://qyh.local/1.png',//营业照片
    license_no:'SD2313123'   //营业执照号码
    parent_industry_id:'1',
    industry_id:'2'

}

成功:
{
    "status": 0,
    "return": "添加成功!",
    "data": "添加成功!"
}

失败:
{
    "status": 1,
    "return": "请选择所属分类!",
    "data": "请选择所属分类!"
}



6.上传图片

POST:{url}/tool/upload_image
参数:
{_TOKEN_:''}
返回:
{file_url: "/uploads/images/20180526/54200900_1527336836.png"}

