1.申请列表
GET:{url}/user/apply
参数{_TOKEN_:''}
例如:
http://qyh.local/user/apply
POST:{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5}
返回:
有数据:
{
    "current_page": 1,
    "data": [
        {
            "id": 1,
            "user_id": 2,
            "company_id": 2,
            "company_user_id": 1,
            "status": 0,
            "money": "10000.00",
            "created_at": "2018-05-26 21:14:59",
            "updated_at": "2018-05-26 21:14:59",
            "user_name": "Band",
            "status_text": "待审核"
        },
        {
            "id": 2,
            "user_id": 3,
            "company_id": 2,
            "company_user_id": 1,
            "status": 1,
            "money": "3000.00",
            "created_at": "2018-05-26 21:14:59",
            "updated_at": "2018-05-26 21:14:59",
            "user_name": "Band",
            "status_text": "已审核"
        }
    ],
    "first_page_url": "http://qyh.local/user/apply?page=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http://qyh.local/user/apply?page=1",
    "next_page_url": null,
    "path": "http://qyh.local/user/apply",
    "per_page": 20,
    "prev_page_url": null,
    "to": 2,
    "total": 2
}

无数据
{
    "current_page": 1,
    "data": [],
    "first_page_url": "http://qyh.local/user/apply?page=1",
    "from": null,
    "last_page": 1,
    "last_page_url": "http://qyh.local/user/apply?page=1",
    "next_page_url": null,
    "path": "http://qyh.local/user/apply",
    "per_page": 20,
    "prev_page_url": null,
    "to": null,
    "total": 0
}

2.获取申请信息
GET:{url}/user/apply/{id}
参数
{_TOKEN_:''}
例如:
http://qyh.local/user/apply/1
数据:
{
    "apply_info": {
        "id": 1,
        "user_id": 2,
        "company_id": 2,
        "company_user_id": 1,
        "status": 2,
        "money": "10000.00",
        "created_at": "2018-05-26 21:14:59",
        "updated_at": "2018-05-26 23:10:32"
    },
    "user": {
        "id": 2,
        "name": "User_2",
        "avatar": "http://wx.qlogo.cn/mmopen/vi_32/aSKcBBPpibyKNicHNTMM0qJVh8Kjgiak2AHWr8MHM4WgMEm7GFhsf8OYrySdbvAMvTsw3mo8ibKicsnfN5pRjl1p8HQ/0",
        "id_card": "",
        "true_name": "",
        "id_sn": "S000002"
    }
}


3.同意或拒绝额度申请 //status 0为未审核,1为已通过2为拒绝
POST:{url}/user/apply/{id}
参数{_TOKEN_:'',status:'',money:'',discount:'1-100'}
例如:
http://qyh.local/user/apply/1
POST:{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5,status:1,money:1000,discount:90}

成功:
{
    "status": 0,
    "return": "操作成功!",
    "data": "操作成功!"
}

失败:
{
    "status": 1,
    "return": "未找到该申请或您已操作!",
    "data": "未找到该申请或您已操作!"
}



4.用户认证
POST:{url}/user/check
参数
{
_TOKEN_:'',
id_card_front:'',
id_card_back:'',
phone:'',
name:'',
id_card:''
}

例如:
http://qyh.local/user/check/
POST:{
_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5,
phone:1333333333,
id_card_front:'https://qyh.local/1.png',
id_card_back:'https://qyh.local/2.png',
name:'张三',
id_card:'375123198502015532'
}

成功:
{
    "status": 0,
    "return": "操作成功，等待审核!",
    "data": "操作成功，等待审核!"
}

失败:
{
    "status": 1,
    "return": "手机号码不正确!",
    "data": "手机号码不正确!"
}



5.交易列表
POST:{url}/user/order
参数
{
_TOKEN_:'',
start_time:'',
end_time:'',
}

例如:
POST:http://qyh.local/user/order
参数
{
_TOKEN_:'',
start_time:'2018-05-06 00:00:00',
end_time:''2018-06-06 00:00:00'',
}

返回：
{
    "current_page": 1,
    "data": [
        {
            "order_id": 4,
            "user_id": 1,
            "company_id": 1,
            "company_user_id": 2,
            "order_total": "100.00",
            "company_index_all": "100.00",
            "company_index_inc": "90.00",
            "company_admin_inc": "10.00",
            "fees": 10,
            "created_at": "2018-05-27 14:03:48",
            "updated_at": "2018-05-27 14:03:48",
            "order_sn": "SN152740102840296",
            "company_name": "测试"
        },
        {
            "order_id": 3,
            "user_id": 1,
            "company_id": 1,
            "company_user_id": 2,
            "order_total": "1.00",
            "company_index_all": "1.00",
            "company_index_inc": "0.90",
            "company_admin_inc": "0.10",
            "fees": 10,
            "created_at": "2018-05-27 14:03:27",
            "updated_at": "2018-05-27 14:03:27",
            "order_sn": "SN152740100799472",
            "company_name": "测试"
        },
        {
            "order_id": 2,
            "user_id": 1,
            "company_id": 1,
            "company_user_id": 2,
            "order_total": "1.00",
            "company_index_all": "1.00",
            "company_index_inc": "0.90",
            "company_admin_inc": "0.10",
            "fees": 10,
            "created_at": "2018-05-27 14:03:22",
            "updated_at": "2018-05-27 14:03:22",
            "order_sn": "SN152740100289430",
            "company_name": "测试"
        }
    ],
    "first_page_url": "http://qyh.local/user/order?page=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http://qyh.local/user/order?page=1",
    "next_page_url": null,
    "path": "http://qyh.local/user/order",
    "per_page": 20,
    "prev_page_url": null,
    "to": 3,
    "total": 3
}


无数据：
{
    "current_page": 1,
    "data": [],
    "first_page_url": "http://qyh.local/user/order?page=1",
    "from": null,
    "last_page": 1,
    "last_page_url": "http://qyh.local/user/order?page=1",
    "next_page_url": null,
    "path": "http://qyh.local/user/order",
    "per_page": 20,
    "prev_page_url": null,
    "to": null,
    "total": 0
}



