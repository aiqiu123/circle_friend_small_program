1.借入列表
POST:{url}/borrow
参数{_TOKEN_:''}
例如:
http://qyh.local/borrow
POST:{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5}
返回:
有数据:
{
    "current_page": 1,
    "data": [
        {
            "from_user_name": "Band",
            "company_name": "测试",
            "id": 1,
            "from_user_id": 2,
            "to_user_id": 1,
            "company_id": 1,
            "borrow_total": "1000.00",
            "lend_total": "0.00",
            "repay_total": "0.00",
            "last_total": "1000.00",
            "repay_at": "2018-05-24 08:34:24",
            "borrow_at": "2018-05-24 08:34:24",
            "created_at": "2018-05-24 08:34:24",
            "updated_at": "2018-05-24 08:34:24"
        },
        {
            "from_user_name": "Band2",
            "company_name": '',       
            "id": 2,
            "from_user_id": 3,
            "to_user_id": 1,
            "company_id": 0,            //为0则是个人借款
            "borrow_total": "1000.00", //总的借款额
            "lend_total": "500.00",    //还款总额
            "repay_total": "100.00",    //销账总额
            "last_total": "400.00",     //剩余借款总额(这个展示在列表)
            "repay_at": "2018-05-24 08:34:24",
            "borrow_at": "2018-05-24 08:34:24",
            "created_at": "2018-05-24 08:34:24",
            "updated_at": "2018-05-24 08:34:24"
        }
    ],
    "first_page_url": "http://qyh.local/borrow?page=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http://qyh.local/borrow?page=1",
    "next_page_url": null,
    "path": "http://qyh.local/borrow",
    "per_page": 20,
    "prev_page_url": null,
    "to": 2,
    "total": 2
}

无数据

{
    "current_page": 1,
    "data": [],
    "first_page_url": "http://qyh.local/borrow?page=1",
    "from": null,
    "last_page": 1,
    "last_page_url": "http://qyh.local/borrow?page=1",
    "next_page_url": null,
    "path": "http://qyh.local/borrow",
    "per_page": 20,
    "prev_page_url": null,
    "to": null,
    "total": 0
}

2.借出列表

POST:{url}/lend
参数{_TOKEN_:''}
例如:
http://qyh.local/lend
POST:{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5}
返回:
有数据:
{
    "current_page": 1,
    "data": [
        {
            "to_user_name": "Band",   //借款人
            "company_name": "测试",
            "id": 1,                //借条ID
            "from_user_id": 1,
            "to_user_id": 2,
            "company_id": 1,
            "borrow_total": "1000.00",
            "lend_total": "0.00",
            "repay_total": "0.00",
            "last_total": "1000.00",
            "repay_at": "2018-05-24 08:34:24",
            "borrow_at": "2018-05-24 08:34:24",
            "created_at": "2018-05-24 08:34:24",
            "updated_at": "2018-05-24 08:34:24"
        },
        {
            "to_user_name": "Band2",
            "company_name": '',       
            "id": 2,
            "from_user_id": 1,
            "to_user_id": 3,
            "company_id": 0,            //为0则是个人借款
            "borrow_total": "1000.00", //总的借款额
            "lend_total": "500.00",    //还款总额
            "repay_total": "100.00",    //销账总额
            "last_total": "400.00",     //剩余借款总额(这个展示在列表)
            "repay_at": "2018-05-24 08:34:24",  //还款日期
            "borrow_at": "2018-05-24 08:34:24", //借款日期
            "created_at": "2018-05-24 08:34:24",
            "updated_at": "2018-05-24 08:34:24"
        }
    ],
    "first_page_url": "http://qyh.local/borrow?page=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http://qyh.local/borrow?page=1",
    "next_page_url": null,
    "path": "http://qyh.local/borrow",
    "per_page": 20,
    "prev_page_url": null,
    "to": 2,
    "total": 2
}

无数据

{
    "current_page": 1,
    "data": [],
    "first_page_url": "http://qyh.local/borrow?page=1",
    "from": null,
    "last_page": 1,
    "last_page_url": "http://qyh.local/borrow?page=1",
    "next_page_url": null,
    "path": "http://qyh.local/borrow",
    "per_page": 20,
    "prev_page_url": null,
    "to": null,
    "total": 0
}


3.借入借条信息
POST:{url}/{id}/borrow
参数{_TOKEN_:''}

例如:
http://qyh.local/1/borrow
POST:{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5}

有数据（先从data里面取数据吧）
{
    "status": 0,
    "return": {
        "from_user_name": "User_2",
        "company_name": "测试",
        "id": 1,
        "from_user_id": 2,
        "to_user_id": 1,
        "company_id": 1,
        "borrow_total": "1000.00",
        "lend_total": "0.00",
        "repay_total": "0.00",
        "last_total": "1000.00",
        "repay_at": "2018-05-24 08:34:24",
        "borrow_at": "2018-05-24 08:34:24",
        "created_at": "2018-05-24 08:34:24",
        "updated_at": "2018-05-24 08:34:24",
        "to_user_name": "Band"
    },
    "data": {
        "from_user_name": "User_2",
        "company_name": "测试",
        "id": 1,
        "from_user_id": 2,
        "to_user_id": 1,
        "company_id": 1,
        "borrow_total": "1000.00",
        "lend_total": "0.00",
        "repay_total": "0.00",
        "last_total": "1000.00",
        "repay_at": "2018-05-24 08:34:24",
        "borrow_at": "2018-05-24 08:34:24",
        "created_at": "2018-05-24 08:34:24",
        "updated_at": "2018-05-24 08:34:24",
        "to_user_name": "Band"
    }
}

无数据
{
    "status": 1,
    "return": "未找到该借条信息!",
    "data": "未找到该借条信息!"
}



4.借出借条信息
POST:{url}/{id}/lend
参数{_TOKEN_:''}

例如:
http://qyh.local/1/lend
POST:{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5}

有数据
{
    "status": 0,
    "return": {
        "to_user_name": "Band",
        "company_name": null,
        "id": 2,
        "from_user_id": 1,
        "to_user_id": 3,
        "company_id": 0,
        "borrow_total": "1000.00",
        "lend_total": "500.00",
        "repay_total": "100.00",
        "last_total": "400.00",
        "repay_at": "2018-05-24 08:34:24",
        "borrow_at": "2018-05-24 08:34:24",
        "created_at": "2018-05-24 08:34:24",
        "updated_at": "2018-05-24 08:34:24",
        "from_user_name": "Band"
    },
    "data": {
        "to_user_name": "Band",
        "company_name": null,
        "id": 2,
        "from_user_id": 1,
        "to_user_id": 3,
        "company_id": 0,
        "borrow_total": "1000.00",
        "lend_total": "500.00",
        "repay_total": "100.00",
        "last_total": "400.00",
        "repay_at": "2018-05-24 08:34:24",
        "borrow_at": "2018-05-24 08:34:24",
        "created_at": "2018-05-24 08:34:24",
        "updated_at": "2018-05-24 08:34:24",
        "from_user_name": "Band"
    }
}

无数据
{
    "status": 1,
    "return": "未找到该借条信息!",
    "data": "未找到该借条信息!"
}



5.销账
POST:{url}/{id}/cancel_lend
参数{_TOKEN_:'',money:'',pay_pass:''}

例如:
http://qyh.local/1/cancel_lend
POST:{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5,money:100,pay_pass:'5435345'}

操作成功:
{
    "status": 0,
    "return": "销账成功!",
    "data": "销账成功!"
}

错误消息:
{
    "status": 1,
    "return": "请填写销账金额!",
    "data": "请填写销账金额!"
}

{
    "status": 1,
    "return": "请填写支付密码!",
    "data": "请填写支付密码!"
}
...

6.账单详情

POST:{url}/{id}/borrow_log
参数{_TOKEN_:''}

例如:
http://qyh.local/1/borrow_log
POST:{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5}

[
    {
        "id": 1,
        "from_user_id": 2,
        "to_user_id": 1,
        "company_id": 0,
        "money_total": "10000.00",  //借款金额
        "money_status": "-10000",   //当前状态
        "iou_type": 0,      //0借入1借出
        "repay_at": "2018-06-23 08:42:13",
        "borrow_at": "2018-05-24 08:42:13",
        "created_at": "2018-05-24 08:42:13",
        "updated_at": "2018-05-24 08:42:13",
        "user_name": "User_2"
    },
    {
        "id": 2,
        "from_user_id": 2,
        "to_user_id": 1,
        "company_id": 0,
        "money_total": "10000.00",
        "money_status": "-20000",
        "iou_type": 0,
        "repay_at": "2018-06-23 08:42:13",
        "borrow_at": "2018-05-24 08:42:13",
        "created_at": "2018-05-24 08:42:13",
        "updated_at": "2018-05-24 08:42:13",
        "user_name": "User_2"
    },
    {
        "id": 3,
        "from_user_id": 2,
        "to_user_id": 1,
        "company_id": 0,
        "money_total": "10000.00",
        "money_status": "-30000",
        "iou_type": 0,
        "repay_at": "2018-06-23 08:42:13",
        "borrow_at": "2018-05-24 08:42:13",
        "created_at": "2018-05-24 08:42:13",
        "updated_at": "2018-05-24 08:42:13",
        "user_name": "User_2"
    },
    {
        "id": 4,
        "from_user_id": 1,
        "to_user_id": 2,
        "company_id": 0,
        "money_total": "10000.00",
        "money_status": "-20000",
        "iou_type": 1,
        "repay_at": "2018-06-23 08:42:13",
        "borrow_at": "2018-05-24 08:42:13",
        "created_at": "2018-05-24 08:42:13",
        "updated_at": "2018-05-24 08:42:13",
        "user_name": "User_2"
    }
]

错误消息:
{
    "status": 1,
    "return": "暂无数据!",
    "data": "暂无数据!"
}
...



7.借出账单详情

POST:{url}/{id}/lend_log
参数{_TOKEN_:''}

例如:
http://qyh.local/1/lend_log
POST:{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5}

[
    {
        "id": 1,
        "from_user_id": 2,
        "to_user_id": 1,
        "company_id": 0,
        "money_total": "10000.00",  //借款金额
        "money_status": "-10000",   //当前状态
        "iou_type": 0,      //0借入1借出
        "repay_at": "2018-06-23 08:42:13",
        "borrow_at": "2018-05-24 08:42:13",
        "created_at": "2018-05-24 08:42:13",
        "updated_at": "2018-05-24 08:42:13",
        "user_name": "User_2"
    },
    {
        "id": 2,
        "from_user_id": 2,
        "to_user_id": 1,
        "company_id": 0,
        "money_total": "10000.00",
        "money_status": "-20000",
        "iou_type": 0,
        "repay_at": "2018-06-23 08:42:13",
        "borrow_at": "2018-05-24 08:42:13",
        "created_at": "2018-05-24 08:42:13",
        "updated_at": "2018-05-24 08:42:13",
        "user_name": "User_2"
    },
    {
        "id": 3,
        "from_user_id": 2,
        "to_user_id": 1,
        "company_id": 0,
        "money_total": "10000.00",
        "money_status": "-30000",
        "iou_type": 0,
        "repay_at": "2018-06-23 08:42:13",
        "borrow_at": "2018-05-24 08:42:13",
        "created_at": "2018-05-24 08:42:13",
        "updated_at": "2018-05-24 08:42:13",
        "user_name": "User_2"
    },
    {
        "id": 4,
        "from_user_id": 1,
        "to_user_id": 2,
        "company_id": 0,
        "money_total": "10000.00",
        "money_status": "-20000",
        "iou_type": 1,
        "repay_at": "2018-06-23 08:42:13",
        "borrow_at": "2018-05-24 08:42:13",
        "created_at": "2018-05-24 08:42:13",
        "updated_at": "2018-05-24 08:42:13",
        "user_name": "User_2"
    }
]

错误消息:
{
    "status": 1,
    "return": "暂无数据!",
    "data": "暂无数据!"
}
...

