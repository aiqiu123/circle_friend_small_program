1.商家列表
GET:
/user/company_list
{_TOKEN_:}
2.删除商家
POST:
/user/company/{id}/del
{_TOKEN_:}
3.修改商家信息
POST:
/user/company/{id}
参数和添加商家相同


4.商家授予额度
POST:
http://{url}/user/company/share/{id}
{_TOKEN_:'',money:'',discount:'',relation:''}   //discount 商家折扣，1-100;relation:关系,用户自己填写(用户共享接口也加上该字段);
例如:
POST: http://qyh.local/user/company/share/2
{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5,money:10000,discount:90,relation:'兄弟'}
成功:
{
    "status": 0,
    "return": "操作成功!",
    "data": "操作成功!"
}

失败:
{
    "status": 1,
    "return": "额度不能低于1元",
    "data": "额度不能低于1元"
}

5.共享额度列表
POST:
/user/share_list/{type}     //type:0普通用户共享额度,1商家共享额度,10:所有共享信息
{_TOKEN_:}